package com.helenacorp.quete_parcelable;

import android.content.ClipData;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] noms = {"Tata", "Marcelle", "Lilian", "Babar", "Lion",
                "Tor"};
        String[] prenoms = {"Ana", "Navette", "So", "Bao", "Ail", "A"};
        int[] prix = {123, 567, 89, 65, 39, 55};

        listView = (ListView) findViewById(R.id.listview);

        ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();

        HashMap<String, String> map;
        map = new HashMap<String, String>();
        map.put("text_prenom", "To");
        map.put("text_nom", "Torro");
        map.put("prix", "123");
        listItem.add(map);

        map = new HashMap<String, String>();
        map.put("text_prenom", "Sarah");
        map.put("text_nom", "Faure");
        map.put("prix", "89");
        listItem.add(map);

        map = new HashMap<String, String>();
        map.put("text_prenom", "Passy");
        map.put("text_nom", "Bayte");
        map.put("prix", "789");
        listItem.add(map);

        map = new HashMap<String, String>();
        map.put("text_prenom", "Corrine");
        map.put("text_nom", "Pas");
        map.put("prix", "90");
        listItem.add(map);

        //SimpleAdapterse chargera de mettre les items présent dans notre list (listItem) dans la vue affichageitem
        SimpleAdapter mSchedule = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.trip_item,
                new String[]{"text_prenom", "text_nom", "prix"}, new int[]{R.id.text_prenom, R.id.text_nom, R.id.prix});

        listView.setAdapter(mSchedule);


    }


}
