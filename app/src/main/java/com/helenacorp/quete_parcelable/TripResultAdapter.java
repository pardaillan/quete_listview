package com.helenacorp.quete_parcelable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by helena on 30/06/2017.
 */

public class TripResultAdapter extends BaseAdapter {
    Context context;
    private ArrayList<TripResultModel> items; //data source of the list adapter

    public TripResultAdapter(Context context, ArrayList<TripResultModel> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.trip_item, parent, false);
        }

        // get current item to be displayed
        TripResultModel currentItem = (TripResultModel) getItem(position);
        // get current item to be displayed

        // get the TextView for item name and item description
        TextView prenom = (TextView) convertView.findViewById(R.id.text_prenom);
        TextView nom = (TextView) convertView.findViewById(R.id.text_nom);
        TextView prix = (TextView) convertView.findViewById(R.id.prix);
        // [...]
        //sets the text for item name and item description from the current item object
        prenom.setText(currentItem.getPrenom());
        nom.setText(currentItem.getNom());
        prix.setText(currentItem.getPrice());

        // returns the view for the current row
        return convertView;

    }
}
