package com.helenacorp.quete_parcelable;

/**
 * Created by helena on 30/06/2017.
 */

public class TripResultModel {
    private String prenom;
    private String nom;
    private int price;

    public TripResultModel(String prenom, String nom, int price) {
        this.prenom = prenom;
        this.nom = nom;
        this.price = price;
    }

    public String getPrenom() {

        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
